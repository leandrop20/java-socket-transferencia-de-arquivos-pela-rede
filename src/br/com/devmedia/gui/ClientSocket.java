package br.com.devmedia.gui;

import br.com.devmedia.bean.Arquivo;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class ClientSocket extends javax.swing.JFrame {

    private static final long serialVersionUID = 1L;
    
    private final long SIZE_ALLOWED_KB = 5120;
    private Socket socket;
    private Arquivo arquivo;
    
    public ClientSocket() {
        this.setTitle("Envio de Arquivo via socket!");
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        initComponents();
        
        txtIP.setText("127.0.0.1");
        txtPort.setText("5566");
        txtDir.setText("C:\\Users\\PINHEIRO\\Desktop");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFileChooser1 = new javax.swing.JFileChooser();
        jLabel1 = new javax.swing.JLabel();
        txtFile = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtIP = new javax.swing.JTextField();
        txtPort = new javax.swing.JTextField();
        txtDir = new javax.swing.JTextField();
        btSelect = new javax.swing.JButton();
        txtSize = new javax.swing.JLabel();
        btSend = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Arquivo Carregado");

        txtFile.setEditable(false);

        jLabel2.setText("IP");

        jLabel3.setText("Porta");

        jLabel4.setText("Diretório Dest.");

        btSelect.setText("Selecionar Arquivo");
        btSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSelectActionPerformed(evt);
            }
        });

        txtSize.setText("Tamanho:");

        btSend.setText("Enviar");
        btSend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSendActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtFile)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtIP)
                            .addComponent(txtPort)
                            .addComponent(txtDir)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btSelect)
                            .addComponent(txtSize)
                            .addComponent(btSend))
                        .addGap(0, 251, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtIP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btSelect)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtSize)
                .addGap(26, 26, 26)
                .addComponent(btSend)
                .addContainerGap(34, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSelectActionPerformed
        FileInputStream fis;
        try {
            JFileChooser chooser = new JFileChooser();
            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            chooser.setDialogTitle("Escolha o arquivo");
            
            if (chooser.showOpenDialog(this) == JFileChooser.OPEN_DIALOG) {
                File fileSelected = chooser.getSelectedFile();
                
                byte[] bFile = new byte[(int) fileSelected.length()];
                fis = new FileInputStream(fileSelected);
                fis.read(bFile);
                fis.close();
                
                long kbSize = fileSelected.length() / 1024;
                txtFile.setText(fileSelected.getName());
                txtSize.setText(kbSize + " KB");
                
                arquivo = new Arquivo();
                arquivo.setConteudo(bFile);
                arquivo.setDataHoraUpload(new Date());
                arquivo.setNome(fileSelected.getName());
                arquivo.setTamanhoKB(kbSize);
                arquivo.setIpDestino(txtIP.getText());
                arquivo.setPortaDestino(txtPort.getText());
                arquivo.setDiretorioDestino(txtDir.getText().trim());
            }
        } catch (Exception e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }//GEN-LAST:event_btSelectActionPerformed

    private void btSendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSendActionPerformed
        if (fieldsOk() && validaArquivo()) {
            try {
                socket = new Socket(
                        txtIP.getText().trim(), Integer.parseInt(txtPort.getText().trim()));
                
                BufferedOutputStream bf = new BufferedOutputStream(socket.getOutputStream());
                
                byte[] bytes = objectToByte();
                bf.write(bytes);
                bf.flush();
                bf.close();
                socket.close();
            } catch (UnknownHostException e) {
                System.out.println("UnknownHostException: " + e.getMessage());
            } catch (IOException e) {
                System.out.println("IOException: " + e.getMessage());
            }
        }
    }//GEN-LAST:event_btSendActionPerformed

    private boolean fieldsOk() {
        String message = "";
        boolean bool = true;
        
        String IPADDRESS_PATTERN = 
        "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
        Pattern p = Pattern.compile(IPADDRESS_PATTERN);
        Matcher m = p.matcher(txtIP.getText());
        if (!m.matches()) {
            message = "Formato de IP invalido: ex ###.###.#.#!";
            bool = false;
            txtIP.requestFocus();
        } else if (txtPort.getText().length() != 4) {
            message = "A Porta deve ser preenchida ex: 0000!";
            bool = false;
            txtPort.requestFocus();
        } else if (txtDir.getText().equals("")) {
            message = "O Destino deve ser preenchido!";
            bool = false;
            txtDir.requestFocus();
        } else if (txtFile.getText().equals("")) {
            message = "Selecione um arquivo!";
            bool = false;
        }
        
        if (!bool) {
            JOptionPane.showMessageDialog(this, message, "Atenção", WIDTH);
        }
        return bool;
    }
    
    private boolean validaArquivo() {
        if (arquivo.getTamanhoKB() > SIZE_ALLOWED_KB) {
            JOptionPane.showMessageDialog(this, 
                    "Tamanho máximo permitido atingido (" + (SIZE_ALLOWED_KB/1024) + ")");
            return false;
        } else {
            return true;
        }
    }
    
    private byte[] objectToByte() {
        try {
            ByteArrayOutputStream bao = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bao);
            
            oos.writeObject(arquivo);
            return bao.toByteArray();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this, "Erro na resialização!");
            System.out.println("IOException: " + e.getMessage());
        }
        return null;
    }
    
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            new ClientSocket().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btSelect;
    private javax.swing.JButton btSend;
    private javax.swing.JFileChooser jFileChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JTextField txtDir;
    private javax.swing.JTextField txtFile;
    private javax.swing.JTextField txtIP;
    private javax.swing.JTextField txtPort;
    private javax.swing.JLabel txtSize;
    // End of variables declaration//GEN-END:variables
}